FROM alpine:3.19

RUN apk add --no-cache \
    helm=3.13.2-r0 \
    kubectl=1.28.4-r0 \
    --repository=http://dl-cdn.alpinelinux.org/alpine/3.19/community
